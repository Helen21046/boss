import os
import unittest

import numpy as np
import pytest
from numpy.testing import assert_allclose, assert_equal

from boss.bo.bo_main import BOMain
from boss.settings import Settings
from boss.io.main_output import MainOutput
from boss.utils.timer import Timer
from boss.bo.rstmanager import RstManager


class BoTest(unittest.TestCase):
    """
    Test BOMain optimization on various toy functions
    """

    def setUp(self):
        path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(path)

    def tearDown(self):
        os.remove("boss.out")
        os.remove("boss.rst")

    def test_gramacy_lee(self):
        """
        #Test BO class optimization on Gramacy & Lee function
        """
        settings = Settings.from_file("inputs/gl.in")
        settings["minzacc"] = 0.75

        bo = BOMain.from_settings(settings)

        bo.run()

        # true global minimum
        x_true = np.array([0.548563])
        y_true = -0.869011

        # BO global minimum prediction
        res = bo.convergence[-1]
        xhat = res[2]
        mu = res[3]

        # prediction should be close to true value
        assert_allclose(x_true, xhat, atol=0.002)
        self.assertAlmostEqual(mu, y_true, delta=0.02)


if __name__ == "__main__":
    unittest.main()
