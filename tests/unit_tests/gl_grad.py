import numpy as np


def f(x):
    return (
        x,
        np.sin(10 * np.pi * x) / (2 * x) + (x - 1) ** 4,
        -np.sin(10 * np.pi * x) / (2 * x ** 2)
        + 4 * (x - 1) ** 3
        + 5 * np.pi * np.cos(10 * np.pi * x) / x,
    )
