import os

import numpy as np
import pytest
from pytest import approx

import boss.keywords as bkw
from boss.settings import Settings
from boss.settings import standardize_signature


@pytest.fixture
def settings_minimal():
    """Minimally initialized settings object for testing __init__. """
    sts = Settings.__new__(Settings)
    super(dict, sts).__init__()
    return sts


@pytest.fixture
def settings():
    sts = Settings.__new__(Settings)
    super(dict, sts).__init__()
    sts.is_rst = False
    sts.dir = os.getcwd()
    sts.acqfn = None
    for cat in bkw.categories.values():
        sts.update(cat)
    sts["bounds"] = np.array([[0.0, 1.0], [0.0, 1.0]])
    return sts


@pytest.fixture(scope="session")
def ipfile(tmp_path_factory):
    """Temporary boss input file for testing. """

    tmp_file = tmp_path_factory.mktemp("scratch") / "boss.in"
    with open(tmp_file, "w") as fd:
        # fd.write("userfn       gl.py\n")
        fd.write("bounds       0.0 3.5; 1.0 3.0\n")
        fd.write("kernel       stdp rbf\n")
        fd.write("#kernel       rbf\n")
        fd.write("\n")
        fd.write("yrange       -5.0 2.0\n")
        fd.write("initpts   10 # comment\n")
        fd.write("iterpts 55 \n")

    return tmp_file


def test_standardize_signature():
    # 1D func
    def f1(x):
        return x + 1.0

    f1_new = standardize_signature(f1, xdim=0)
    assert f1_new(np.array([[1.0]])) == approx(2.0)

    # 2D func with two args
    def f2(x, y):
        return x + y + 1.0

    f2_new = standardize_signature(f2, xdim=0)
    assert f2_new(np.array([[1.0, 2.0]])) == approx(4.0)

    # 2D func with one vector arg
    def f3(x):
        return x[0] + x[1] + 1.0

    f3_new = standardize_signature(f3, xdim=1)
    assert f3_new(np.array([[1.0, 2.0]])) == approx(4.0)

    # 4D func with two vector args
    def f4(x, y):
        return x[0] + x[1] + y[0] + y[1] + 1.0

    f4_new = standardize_signature(f4, xdim=1)
    assert f4_new(np.array([[1.0, 1.0, 1.0, 1.0]])) == approx(5.0)

    # 4D func with one matrix arg
    def f5(x):
        return x[0, 0] + x[0, 1] + x[0, 2] + x[0, 3] + 1.0

    f5_new = standardize_signature(f5, xdim=2)
    assert f5_new(np.array([[1.0, 1.0, 1.0, 1.0]])) == approx(5.0)

    # 4D func with two matrix arg
    def f6(x, y):
        return x[0, 0] + x[0, 1] + y[0, 0] + y[0, 1] + 1.0

    f6_new = standardize_signature(f6, xdim=2)
    assert f6_new(np.array([[1.0, 1.0, 1.0, 1.0]])) == approx(5.0)


def test_set_independent_defaults(settings_minimal):

    # restore all values to their defaults
    sts = settings_minimal
    sts.set_independent_defaults(only_missing=False)
    for cat in bkw.get_copied_categories():
        for key, val in cat.items():
            np.testing.assert_equal(sts[key], val)

    # only restore None-valued keywords to their default.
    sts.clear()
    sts["iterpts"] = 100
    sts.set_independent_defaults(only_missing=True)
    assert sts["iterpts"] == 100

    for cat in bkw.get_copied_categories():
        for key, val in cat.items():
            if key != "iterpts":
                np.testing.assert_equal(sts[key], val)


def test_set_acqfn(settings_minimal):
    sts = settings_minimal
    sts["acqfnpars"] = np.array([])
    for name in ["elcb", "lcb", "explore", "exploit", "ei"]:
        sts.set_acqfn(name)
        assert sts.acqfn.__name__ == name
        assert callable(sts.acqfn)


def test_correct(settings_minimal):
    sts = settings_minimal
    sts["pp_acq_funcs"] = True
    sts["verbosity"] = 1
    sts["bounds"] = np.array([[0.0, 1.0], [0.0, 1.0]])
    sts["kernel"] = "rbf"
    sts["gm_tol"] = [0.01, 10.0]
    sts.correct()
    assert sts["kernel"] == ["rbf"] * 2
    assert sts["verbosity"] == 2
    assert sts["gm_tol"][0] == approx(0.01)
    assert sts["gm_tol"][1] == 10


def test_dim_getter(settings):
    assert settings.dim == 2


def test_dim_setter(settings):
    with pytest.raises(AttributeError):
        settings.dim = 3


def test_set_dependent_defaults(settings):
    sts = settings.copy()

    # Restore all dependent keywords to their defaults.
    # Use a non-periodic kernel and a gamma prior
    sts["kernel"] = ["rbf", "rbf"]
    sts.set_dependent_defaults(only_missing=False)
    assert sts["iterpts"] == 42
    np.testing.assert_equal(sts["pp_iters"], np.arange(0, 43))
    assert sts["periods"] == approx(np.array([1.0, 1.0]))
    assert sts["min_dist_acqs"] == approx(0.01)
    assert sts["thetainit"] == approx(np.array([10.0, 0.05, 0.05]))
    assert sts["thetabounds"] == approx(
        np.array([[1e-02, 1e4], [5e-4, 5e0], [5e-4, 5e0]])
    )
    np.testing.assert_array_equal(sts["pp_model_slice"], np.array([1, 2, 25]))

    # Only set defaults for None-valued, dependent keywords.
    # Use a periodic kernel
    sts = settings
    sts["kernel"] = ["stdp", "stdp"]
    sts["periods"] = np.array([0.5, 0.5])
    sts["iterpts"] = 100
    sts["pp_iters"] = np.arange(5)
    sts.set_dependent_defaults(only_missing=True)
    assert sts["periods"] == approx(np.array([0.5, 0.5]))
    assert sts["iterpts"] == 100
    np.testing.assert_array_equal(sts["pp_iters"], np.arange(5))
    assert sts["min_dist_acqs"] == approx(0.01 * 0.5)
    assert sts["thetainit"] == approx(
        np.array([10.0, 0.3141592653589793, 0.3141592653589793])
    )


def test_init():
    keywords = {
        "bounds": np.array([[0.0, 1.0], [0.0, 1.0]]),
        "verbosity": 1,
        "kernel": "rbf",
    }
    sts = Settings(keywords)
    assert sts["bounds"] == approx(keywords["bounds"])
    assert sts["kernel"] == [keywords["kernel"]] * 2
    assert sts["verbosity"] == keywords["verbosity"]
    assert sts["iterpts"] == 42
    assert sts["thetaprior"] == "gamma"


def test_from_file(ipfile):
    sts = Settings.from_file(ipfile)
    assert sts.is_rst == False
    assert sts["bounds"] == approx(np.array([[0.0, 3.5], [1.0, 3.0]]))
    assert sts["kernel"] == ["stdp", "rbf"]
    assert sts["iterpts"] == 55
    assert sts["initpts"] == 10
    assert sts["yrange"] == approx(np.array([-5.0, 2.0]))
