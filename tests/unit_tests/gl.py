import numpy as np


def f(x):
    return np.sin(10 * np.pi * x) / (2 * x) + (x - 1) ** 4
