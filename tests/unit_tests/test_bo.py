import os
import pytest
from pytest import approx

import numpy as np

from boss.bo.bo_main import BOMain
from boss.settings import Settings
from boss.io.main_output import MainOutput
from boss.bo.rstmanager import RstManager


@pytest.fixture
def bo_from_file():
    cwd = os.getcwd()
    path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(path)
    bo = BOMain.from_file("inputs/methods.in")
    yield bo
    try:
        os.remove("boss.out")
        os.remove("boss.rst")
    except FileNotFoundError:
        pass
    os.chdir(cwd)


def test_add_xy_method(bo_from_file):
    """
    Test adding data using BOMain class method
    """
    bo = bo_from_file
    x0 = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y0 = np.array([[0], [1], [1], [2]])

    bo.add_xy_list(x0, y0)

    x1 = bo.X_init
    y1 = bo.Y_init

    # model should contain only x0 and normalized y0 as data
    assert x1 == approx(x0)
    assert y1 == approx(y0)


def test_add_xy_ipfile(bo_from_file):
    """
    Test adding data using input file
    """
    bo = bo_from_file
    bo.run()

    # values in input file methods.in
    x0 = np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0], [0.5, 0.5]])
    y0 = np.array([[0.0], [1.0], [1.0], [2.0], [1.0]])

    x1 = bo.X_init
    y1 = bo.Y_init

    # initialized model should contain only x0, y0 as data
    assert x1 == approx(x0)
    assert y1 == approx(y0)


def test_get_xy(bo_from_file):
    """
    Test getting data using BOMain class methods
    """
    bo = bo_from_file
    x0 = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    y0 = np.array([[0], [1], [1], [2]])

    bo.add_xy_list(x0, y0)

    x1 = bo.get_x()
    y1 = bo.get_y()

    assert x1 == approx(x0)
    assert y1 == approx(y0)


def test_init_model(bo_from_file):
    """
    Test initializing model and prediction with initialized model
    """
    bo = bo_from_file
    # initialize model by adding all initial points in input file
    bo.run()

    # model should be initialized
    assert bo.model is not None

    x0 = bo.get_x()
    y0 = bo.get_y()
    mu = bo.get_mu(x0)
    nu = bo.get_nu(x0)

    # model should predict approximately mu == y0 at x0
    for i in range(len(y0)):
        assert mu[i] == approx(y0[i], abs=nu[i])
