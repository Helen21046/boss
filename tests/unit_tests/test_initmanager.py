import unittest
from unittest.mock import Mock

import numpy as np

from boss.bo.initmanager import InitManager


class InitManagerTest(unittest.TestCase):
    """
    Test cases for InitManager class
    """

    def setUp(self):
        """
        Initialize set of test bounds
        """
        self.bounds = np.asarray(
            [
                np.array([[-2, 4], [-3, 6], [8, 17]]),
                np.array([[-56, 43]]),
                np.array([[-1.1, 0.18], [65.1, 68.87]]),
            ]
        )

    def _test_init(self, inittype, expected_pts):
        """
        method for testing that InitManager inittype returns correct number
        (as specified by function expected_pts) of valid points.
        """
        for bound in self.bounds:
            nof_pts = 8
            dim = len(bound)
            initmgr = InitManager(inittype, bound, nof_pts)
            points = initmgr.get_all()

            # expect correct number of points
            self.assertEqual(len(points), expected_pts(nof_pts, dim))

            for pnt in points:
                # expect: all points within bounds
                with self.subTest("bounds:\n" + str(bound) + ",\npoint: " + str(pnt)):
                    self.assertTrue(np.all(bound[:, 0] <= pnt))
                    self.assertTrue(np.all(bound[:, 1] >= pnt))

    def test_init_random(self):
        self._test_init("random", lambda x, d: x)

    def test_init_sobol(self):
        self._test_init("sobol", lambda x, d: x)

    def test_init_grid(self):
        self._test_init("grid", lambda x, d: int(np.power(x, 1.0 / d)) ** d)


if __name__ == "__main__":
    unittest.main()
