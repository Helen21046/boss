.. BOSS documentation master file, created by
   sphinx-quickstart on Tue Dec 12 14:46:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Module Documentation
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

File: __main__.py
--------------------------------
.. automodule:: boss.__main__
.. autofunction:: main

File: bo/acq/ei.py
--------------------------------
.. automodule:: boss.bo.acq.ei
.. autofunction:: ei

File: bo/acq/elcb.py
--------------------------------
.. automodule:: boss.bo.acq.elcb
.. autofunction:: elcb

File: bo/acq/exploit.py
--------------------------------
.. automodule:: boss.bo.acq.exploit
.. autofunction:: exploit

File: bo/acq/explore.py
--------------------------------
.. automodule:: boss.bo.acq.explore
.. autofunction:: explore

File: bo/acq/lcb.py
--------------------------------
.. automodule:: boss.bo.acq.lcb
.. autofunction:: lcb

File: bo/bo_main.py
--------------------------------
.. automodule:: boss.bo.bo_main
.. autoclass:: BOMain
   :members:
   :private-members:

File: bo/initmanager.py
--------------------------------
.. automodule:: boss.bo.initmanager
.. autoclass:: InitManager
   :members:
   :private-members:

File: bo/kernel_factory.py
--------------------------------
.. automodule:: boss.bo.kernel_factory
   :members:
   :private-members:

File: bo/model.py
--------------------------------
.. automodule:: boss.bo.model
.. autoclass:: Model
   :members:
   :private-members:

File: bo/rstmanager.py
--------------------------------
.. automodule:: boss.bo.rstmanager
.. autoclass:: RstManager
   :members:
   :private-members:

File: io/dump.py
--------------------------------
.. automodule:: boss.io.dump
   :members:
   :private-members:

File: io/ioutils.py
--------------------------------
.. automodule:: boss.io.ioutils
   :members:
   :private-members:

File: io/main_output.py
--------------------------------
.. automodule:: boss.io.main_output
.. autoclass:: MainOutput
   :members:
   :private-members:

File: io/parse.py
--------------------------------
.. automodule:: boss.io.parse
   :members:
   :private-members:

File: keywords.py
--------------------------------
.. automodule:: boss.keywords
   :members:
   :private-members:

File: pp/pp_main.py
--------------------------------
.. automodule:: boss.pp.pp_main
.. autofunction:: PPMain

File: pp/plot.py
--------------------------------
.. automodule:: boss.pp.plot
   :members:
   :private-members:

File: settings.py
--------------------------------
.. automodule:: boss.settings
.. autoclass:: Settings
   :members:
   :private-members:
   :special-members:

File: utils/distributions.py
--------------------------------
.. automodule:: boss.utils.distributions
   :members:
   :private-members:

File: utils/minimization.py
--------------------------------
.. automodule:: boss.utils.minimization
   :members:
   :private-members:

File: utils/timer.py
--------------------------------
.. automodule:: boss.utils.timer
.. autoclass:: Timer
   :members:
   :private-members:
