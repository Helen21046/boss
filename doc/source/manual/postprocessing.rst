BO postprocessing
====================
Much data is collected during the BO process. At each iteration, we
monitor the convergence of the predicted global minimum points and
compare them to data acquisitions, model fits are recorded via the
hyperparameters and acquisition functions are stored. 

The accumulated data can be visualised afterwards by running 
postprocessing routines, as described :ref:`elsewhere<BOSS inputs and outputs>`.
Data in form of raw acsii files and graph is stored
in the ``postprocessing`` folder.
BOSS postprocessing includes the following analysis:

* Acquisition tracking and statistics (default)
* Global minima tracking (:math:`\hat{x}` locations) and convergence
  of :math:`\hat{x}` and :math:`\mu(\hat{x})` (default)
* GP model hyperparameter tracking (default)
* True function evaluation at global minimum prediction :math:`f(\hat{x})`
* GP model visualisation in 1D or 2D cross-sections, at global minimum 
  (default) or any other location in phase space.
* True function evaluation on grids across the entire phase space 
  (for model validation, not recommended in N>2D.)

.. raw:: html

   <h2>Data-mining the surrogate model</h2>


After the run, it is possible to reconstruct the
GP surrogate models from the iterative process: these present a major
resource and we have developed advanced postprocessing routines to
extract further information such as:

* **Local minima.** The GP model represents an N-dimensional property
  landscape defined across the variable domain :math:`\mathcal{X}`, and
  it is possible to traverse it for information. While it can never be
  fully known (high information content), we can compute its value at
  any state :math:`x_i \in \mathcal{X}`. This allows use of standard
  minimiser routines (bfgs, etc) to determine local minima. These
  functions typically follow local gradients to the nearest minimum so
  the initial point on the landscape matters. We conduct many
  minimisation searches starting from a fraction of the lowest energy
  data acquisitions (see section `8 <#key>`__). Acquisitions span the
  :math:`\mathcal{X}` domain space relatively well and this approach
  allows us to identify many distinct minima in N-dimensions, although
  many duplicate minima are found as well. Nominal cleaning of duplicate
  minima is performed, it is up to the user to carefully review the
  output minima list.
* **Minimum Energy Paths (MEP).** The GP model presents us with a
  relatively fast way of analyzing paths on the domain
  :math:`\mathcal{X}`. Paths can be constructed by multidimensional
  pathfinding methods, taking into account the whole state space,
  instead of relying only on some select points (or images, as they are
  sometimes known). BOSS has a postprocessing method for finding paths
  with the lowest maximum energy. This is used by first searching for
  the local minima (running postprocessing with the ``pp_local_minima``
  keyword), and using then the command
  ``boss m <rst-file> <minima-file>``. It is a good idea to first check
  the minima produced by the local minima search to avoid unnecessary or
  duplicate minima from being given as input.

