Tutorials
=========
BOSS supports running either via a simple command-line interface (CLI) or directly in Python scripts using the Python API. Several tutorials are available below for both 
modes of running, with emphasis on the practicalities of using the code. In both modes, BOSS settings such as the number of iterations, key algorihtms, variable input and so on, can be conveniently controlled through BOSS :ref:`keywords<Keywords>`, some of which are necessary to start the active learning simulation. 

Command-line interface
++++++++++++++++++++++
The BOSS CLI is provided by an executable called ``boss`` that is automatically generated during the :ref:`installation<Installation>` process. The function to be optimized must be defined in Python, but once this is done, the user is free to run BOSS from the command line without interacting with Python at all. This is the recommended way to run BOSS on high-performance cluster (HPC) environments. To get started with the BOSS binary, see the following pages:

.. toctree::
   :maxdepth: 4
   
   tutorials/quickstart_cli
   tutorials/postprocessing_cli
   tutorials/restarting_cli

A set of more in-depth tutorials are also available for download:

* :download:`Tutorial 1 <tutorials/cli/tutorial_1.zip>`: simple introduction to BOSS input files, user function and postprocessing analysis.
* :download:`Tutorial 2 <tutorials/cli/tutorial_2.zip>`: alanine conformer search in 2D and 4D (using the `AMBER <https://ambermd.org/>`_ code).
* :download:`Tutorial 3 <tutorials/cli/tutorial_3.zip>`: using existing data (or restarting) to find minima and illustrate landscapes. 

Python API
++++++++++
The following tutorials are currently available for the Python API:

.. toctree::
   :maxdepth: 4
   
   tutorials/quickstart_py
   tutorials/postprocessing_py
   tutorials/resuming_py
   tutorials/restarting_py

Further reading
+++++++++++++++
If you wish to learn more about Bayesian optimization, read the `book by Rasmussen & Williams <http://www.gaussianprocess.org/gpml/chapters/RW.pdf>`_ and `Brochu's BO tutorial <https://arxiv.org/pdf/1012.2599.pdf>`_.
